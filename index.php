<!DOCTYPE html>
<html lang="es">
  <head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,200;1,400;1,500;1,700&display=swap" rel="stylesheet">
    <link rel="preload" href="css/normalize.css" as="style">
    <link rel="stylesheet" href="css/normalize.css">

    <link rel="preload" href="css/style.css" as="style">
    <link rel="stylesheet" href="css/style.css">
    <meta charset="utf-8">
    <title>LIBROS</title>
  </head>
  <body>
      <?php
        include "vista/inicio.php";
      ?>
  </body>
</html>
